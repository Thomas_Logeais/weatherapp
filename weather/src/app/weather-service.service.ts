import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  apiKey='90a251723501bf510f50090be40c1537';
  url;

  constructor(private http: HttpClient) {
    this.url='https://api.openweathermap.org/data/2.5/weather?q='
   }

   getWeather(city: string){
    return this.http.get(this.url + city + ",ca&appid=" + this.apiKey).pipe(map(res => res));
   }
}
