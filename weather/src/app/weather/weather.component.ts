import { HttpClient, HttpResponse, JsonpClientBackend } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { WeatherService } from '../weather-service.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  myControl: FormControl = new FormControl();
  city: string = "";
  weather: any;
  tempC = 0;
  tempF = 0;
  cities : string[] = [];

  constructor(private weatherService:WeatherService) {}
  options : string[] = ['Ottawa', 'Toronto', 'Montreal', 'Vancouver', 'Laval', 'Québec', 'Winnipeg', 'Calgary'];

  ngOnInit(): void {
    this.city = "Montreal";
    this.weatherService.getWeather(this.city).subscribe((response)=>{
      console.log(response);
      this.weather = response;})
  }

  add(){
    const weatherElement = document.getElementById('weather') as HTMLDivElement;
    const errorElement = document.getElementById('error') as HTMLDivElement;
    weatherElement.style.display = "none";
    this.city = this.myControl.value;
    this.weatherService.getWeather(this.city).subscribe((response)=>{
      console.log(response);
      this.weather = response;
      if (this.weather.name != this.city){
        weatherElement.style.display = "none";
        errorElement.style.display = "block";
      }else{
        weatherElement.style.display = "block";
        errorElement.style.display = "none";
      }
      this.changeTemp();
    console.log(this.city);  })

  
     
  }

  changeTemp(){
    this.tempC = Math.round((this.weather.main.temp - 273.15)*100) / 100;
    this.tempF = Math.round(((this.weather.main.temp - 273.15) * 9/5 + 32)*100) / 100;
  }
}
